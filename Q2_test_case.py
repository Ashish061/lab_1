import unittest
from binary_search import binary_search
from linear_search import linear_search


class TestSearch(unittest.TestCase):

    def test_search(self):
        data = [1, 2, 3, 5, 6, 12, 7, 4, 8]
        data1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11]
        self.assertEqual(linear_search(data, 6), 4)
        self.assertEqual(linear_search(data, 10),  -1)
        self.assertEqual(binary_search(data1,0,len(data1)-1, 6), 5)
        self.assertEqual(binary_search(data1,0,len(data1)-1, 12),  -1)

    def test_searchChar(self):
        data = ['t', 'a', 'b', 'l', 'e']
        self.assertEqual(linear_search(data, 'a'), 1)
        
        



if __name__ == "__main__":
    unittest.main()