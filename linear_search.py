def linear_search(data, x):
    for i in range(len(data)):
        if data[i] == x:
            return i

    return -1
  